package com.supertech.heady.utils

import android.content.Context
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import java.util.*

class APICall(url: String?, context: Context?) {
    private var mOnVolleyResponseListener: OnVolleyResponseListener? = null
    fun setOnVolleyResponseListener(mOnVolleyResponseListener: OnVolleyResponseListener?) {
        this.mOnVolleyResponseListener = mOnVolleyResponseListener
    }

    interface OnVolleyResponseListener {
        fun onSuccess(response: String?)
        fun onError(error: String?)
    }

    init {
        val requestQueue = Volley.newRequestQueue(context)
        val stringRequest: StringRequest = object : StringRequest(
            Method.GET, url,
            Response.Listener { response ->
                mOnVolleyResponseListener!!.onSuccess(response)
            },
            Response.ErrorListener { error: com.android.volley.VolleyError ->
                mOnVolleyResponseListener!!.onError(error.toString())
            }) {

        }
        requestQueue.add(stringRequest)
    }
}

