package com.supertech.heady;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.rd.PageIndicatorView;
import com.supertech.heady.adapters.ViewPagerAdapter;
import com.supertech.heady.models.Products;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        getSupportActionBar().hide();
        TextView txt_name = findViewById(R.id.txt_name);
        TextView txt_tax = findViewById(R.id.txt_tax);
        ViewPager pager = findViewById(R.id.vp_varient);

        Intent mIntent = getIntent();
        Products product = (Products) mIntent.getSerializableExtra("SINGLE");
        PageIndicatorView pageIndicatorView = findViewById(R.id.pageIndicatorView);
        pageIndicatorView.setCount(product.getVariants().size()); // specify total count of indicators
        pageIndicatorView.setSelection(0);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager(), product.getVariants());
        pager.setAdapter(adapter);

        txt_name.setText(product.getName());
        txt_tax.setText("Other taxes: "+product.getTax().getName() + "-" + product.getTax().getValue());

    }
}
