package com.supertech.heady.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.supertech.heady.R;
import com.supertech.heady.models.Variants;

public class Varient extends Fragment {

    private Variants variants;

    public Varient(Variants variants) {
        this.variants = variants;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.lay_varient, container, false);
        TextView txt_price = view.findViewById(R.id.txt_price);
        TextView txt_size = view.findViewById(R.id.txt_size);
        TextView txt_color = view.findViewById(R.id.txt_color);

        txt_color.setText(variants.getColor());
        txt_price.setText("Rs."+variants.getPrice()+"/-");
        txt_size.setText(variants.getSize()+" Inch.");

        return view;
    }
}
