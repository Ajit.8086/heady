package com.supertech.heady.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Tax implements Serializable {
    private String name;

    private String value;

    protected Tax(Parcel in) {
        name = in.readString();
        value = in.readString();
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
