package com.supertech.heady.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Variants  implements Serializable {
    private String color;

    private String size;

    private String price;

    private String id;

    protected Variants(Parcel in) {
        color = in.readString();
        size = in.readString();
        price = in.readString();
        id = in.readString();
    }



    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
