package com.supertech.heady.models;

import java.util.ArrayList;

public class ProductList {
    private ArrayList<Rankings> rankings;

    private ArrayList<Categories> categories;

    public ArrayList<Rankings> getRankings() {
        return rankings;
    }

    public void setRankings(ArrayList<Rankings> rankings) {
        this.rankings = rankings;
    }

    public ArrayList<Categories> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<Categories> categories) {
        this.categories = categories;
    }
}
