package com.supertech.heady.models;

import android.content.Intent;

import com.google.gson.Gson;
import com.supertech.heady.ProductActivity;

import java.util.ArrayList;

public class Categories {
    private String name;

    private int  rank_count;

    public int getRank_count() {
        return rank_count;
    }

    public void setRank_count(int rank_count) {
        this.rank_count = rank_count;
    }

    private String id;

    private ArrayList<String> child_categories;

    private ArrayList<Products> products;

    public Categories(String name, String id, ArrayList<String> child_categories, ArrayList<Products> products) {
        this.name = name;
        this.id = id;
        this.child_categories = child_categories;
        this.products = products;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<String> getChild_categories() {
        return child_categories;
    }

    public void setChild_categories(ArrayList<String> child_categories) {
        this.child_categories = child_categories;
    }

    public ArrayList<Products> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Products> products) {
        this.products = products;
    }
}
