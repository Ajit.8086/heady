package com.supertech.heady.models;

import java.util.ArrayList;

public class Rankings {
    private String ranking;

    private ArrayList<Products> products;

    public String getRanking() {
        return ranking;
    }

    public void setRanking(String ranking) {
        this.ranking = ranking;
    }

    public ArrayList<Products> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Products> products) {
        this.products = products;
    }
}
