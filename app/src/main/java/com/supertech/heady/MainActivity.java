package com.supertech.heady;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.supertech.heady.adapters.ProductAdapter;
import com.supertech.heady.models.Categories;
import com.supertech.heady.models.ProductList;
import com.supertech.heady.models.Products;
import com.supertech.heady.utils.APICall;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MainActivity extends AppCompatActivity {

    private RecyclerView rv_products;
    ImageView iv_menus;
    ProductList objList;
    PopupMenu popup;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);
        rv_products = findViewById(R.id.rv_products);
        iv_menus = findViewById(R.id.iv_menus);
        popup = new PopupMenu(MainActivity.this, iv_menus);
        callAPI();
        iv_menus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopup();

            }
        });

    }

    private void showPopup() {
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                sort(item.getItemId());

                return true;
            }
        });

        popup.show();
    }

    private void sort(int itemId) {

        if (itemId == 0) {
            setAdapter(objList.getCategories());
            return;
        } else {
            itemId = itemId - 1;
        }

        if (null != objList) {
            ArrayList<Products> productList = objList.getRankings().get(itemId).getProducts();
            ArrayList<Categories> MainListTemp = new ArrayList<>();
            ArrayList<Categories> MainList = objList.getCategories();

            for (int i = 0; i < productList.size(); i++) {
                for (int j = 0; j < MainList.size(); j++) {
                    if (productList.get(i).getId().equalsIgnoreCase(MainList.get(j).getId())) {
                        MainList.get(j).setRank_count(productList.get(i).getView_count());
                        MainListTemp.add(MainList.get(j));

                    }
                }
            }

            Collections.sort(MainListTemp, new Comparator<Categories>() {
                public int compare(Categories obj1, Categories obj2) {
                    return obj1.getRank_count() - (obj2.getRank_count()); // To compare string values
                }
            });


            setAdapter(MainListTemp);


        }

    }


    private void callAPI() {
        APICall call = new APICall("https://stark-spire-93433.herokuapp.com/json", this);
        Log.d("url", "call");
        call.setOnVolleyResponseListener(new APICall.OnVolleyResponseListener() {
            @Override
            public void onSuccess(@org.jetbrains.annotations.Nullable String response) {
                Log.d("url", "Success");
                objList = new Gson().fromJson(response, ProductList.class);
                setAdapter(objList.getCategories());

                setPopoup();
            }

            @Override
            public void onError(@org.jetbrains.annotations.Nullable String error) {

            }


        });
    }

    private void setPopoup() {
        if (null != objList) {
            popup.getMenu().add(0, 0, 0, "All");
            for (int i = 0; i < objList.getRankings().size(); i++) {
                popup.getMenu().add(0, i + 1, i + 1, objList.getRankings().get(i).getRanking());
            }

        }
    }

    public void gotoProduct(ArrayList<Products> products) {
        try {
            Intent mIntent = new Intent(MainActivity.this, ProductActivity.class);
            mIntent.putExtra("ARRAYLIST", products);
            startActivity(mIntent);
        } catch (Exception w) {
            w.printStackTrace();

        }


    }


    void setAdapter(ArrayList<Categories> lst) {
        try {
            ProductAdapter adapter = new ProductAdapter(lst);
            adapter.setOnClick(MainActivity.this);
            GridLayoutManager manager = new GridLayoutManager(MainActivity.this, 2);
            rv_products.setLayoutManager(manager);
            rv_products.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}


