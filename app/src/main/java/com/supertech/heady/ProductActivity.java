package com.supertech.heady;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.supertech.heady.adapters.CategoryAdapter;
import com.supertech.heady.models.Products;

import java.util.ArrayList;

public class ProductActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);
        getSupportActionBar().hide();
        RecyclerView rv_productList=findViewById(R.id.rv_productList);
        RecyclerView.LayoutManager  manager=new GridLayoutManager(this,2);
        rv_productList.setLayoutManager(manager);
        Intent mIntent=getIntent();
        ArrayList<Products> lst= (ArrayList<Products>) mIntent.getSerializableExtra("ARRAYLIST");
        CategoryAdapter adapter=new CategoryAdapter(lst);
        adapter.setOnClick(this);
        rv_productList.setAdapter(adapter);


    }

    public void gotoDetail(Products singleProduct){

        Intent mIntent=new Intent(this,DetailActivity.class);
        mIntent.putExtra("SINGLE",singleProduct);
        startActivity(mIntent);
    }
}
