package com.supertech.heady.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.supertech.heady.MainActivity
import com.supertech.heady.R
import com.supertech.heady.models.Categories
import java.util.*

class ProductAdapter(private val arylst: ArrayList<Categories>) :
    RecyclerView.Adapter<ProductAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_category, parent, false)
        return ViewHolder(view);
    }

    override fun getItemCount(): Int {

        return arylst.size;
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.txt_name.setText(arylst.get(position).name)
    }

    lateinit var mainActivity :MainActivity
    fun setOnClick(mainActivity: MainActivity) {
        this.mainActivity=mainActivity;
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txt_name: TextView

        init {
            txt_name = itemView.findViewById(R.id.txt_name)
            itemView.setOnClickListener{
                mainActivity.gotoProduct(arylst.get(adapterPosition).products);
            }
        }
    }

}