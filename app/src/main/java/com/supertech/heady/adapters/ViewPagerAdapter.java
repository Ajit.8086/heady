package com.supertech.heady.adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;

import com.supertech.heady.Fragments.Varient;
import com.supertech.heady.models.Variants;

import java.util.ArrayList;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private ArrayList<Variants> variants;

    public ViewPagerAdapter(@NonNull FragmentManager fm, ArrayList<Variants> variants) {
        super(fm);
        this.variants = variants;
    }


    @NonNull
    @Override
    public Fragment getItem(int position) {
        return new Varient(variants.get(position));
    }

    @Override
    public int getCount() {
        return variants.size();
    }
}
