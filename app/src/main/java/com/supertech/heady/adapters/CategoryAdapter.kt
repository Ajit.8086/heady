package com.supertech.heady.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.supertech.heady.ProductActivity
import com.supertech.heady.R
import com.supertech.heady.models.Products
import java.util.*

class CategoryAdapter(private val arylst: ArrayList<Products>) :
    RecyclerView.Adapter<CategoryAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_product, parent, false)
        return ViewHolder(view);
    }

    override fun getItemCount(): Int {

        return arylst.size;
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.txt_name.setText(arylst.get(position).name)
        holder.txt_name.isSelected=true
    }

    lateinit var mainActivity : ProductActivity
    fun setOnClick(mainActivity: ProductActivity) {
        this.mainActivity=mainActivity;
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txt_name: TextView

        init {
            txt_name = itemView.findViewById(R.id.txt_name)
            itemView.setOnClickListener{
                mainActivity.gotoDetail(arylst.get(adapterPosition));
            }
        }
    }

}